package com.oriolgarcia.tvshows.api_client;

import com.oriolgarcia.tvshows.api_client.response.APIError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Error Utils for API Error handling
 * Created by oriolSG on 15/02/2018 for StudioGenesis.
 */

public class ErrorUtils {
    private static final String BASE_URL = "https://api.themoviedb.org/3/tv/";

    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                RetrofitClient.getClient(BASE_URL)
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }
}
