package com.oriolgarcia.tvshows.api_client;

import com.oriolgarcia.tvshows.api_client.response.TvShowsListResponse;
import com.oriolgarcia.tvshows.controllers.models.TvShow;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * ApiService interface with our client api calls to TMDb API
 * Created by Oriol on 10/02/2018.
 */

public interface ApiService {
    @GET("popular")
    Call<TvShowsListResponse> getTvPopularShows(@Query("api_key") String apiKey,
                                                @Query("page") int page);

    @GET("{tv_id}")
    Call<TvShow> getShowDetails(@Path("tv_id") int tvId,
                                @Query("api_key") String apiKey);

    @GET("{tv_id}/similar")
    Call<TvShowsListResponse> getSimilarShows(@Path("tv_id") int tvId,
                                              @Query("api_key") String apiKey);
}
