package com.oriolgarcia.tvshows.api_client.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.oriolgarcia.tvshows.controllers.models.TvShow;

import java.util.ArrayList;

/**
 * Class to serialize the response from the API
 * Created by Oriol on 10/02/2018.
 */

public class TvShowsListResponse {

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("results")
    @Expose
    private ArrayList<TvShow> results = null;
    @SerializedName("total_results")
    @Expose
    private Integer totalResults;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public ArrayList<TvShow> getResults() {
        return results;
    }

    public void setResults(ArrayList<TvShow> results) {
        this.results = results;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

}
