package com.oriolgarcia.tvshows.api_client;

/**
 * Api utils
 * Created by Oriol on 10/02/2018.
 */

public class ApiUtils {

    private static final String BASE_URL = "https://api.themoviedb.org/3/tv/";

    public static ApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL).create(ApiService.class);
    }

}
