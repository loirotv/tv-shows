package com.oriolgarcia.tvshows.controllers.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oriolgarcia.tvshows.R;
import com.oriolgarcia.tvshows.api_client.ErrorUtils;
import com.oriolgarcia.tvshows.api_client.response.APIError;
import com.oriolgarcia.tvshows.api_client.response.TvShowsListResponse;
import com.oriolgarcia.tvshows.controllers.adapters.TvShowsListAdapter;
import com.oriolgarcia.tvshows.controllers.models.Genre;
import com.oriolgarcia.tvshows.controllers.models.ShowCreator;
import com.oriolgarcia.tvshows.controllers.models.TvShow;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ShowDetailsFragment
 * Created by OriolGarcia on 12/2/18.
 */

public class ShowDetailsFragment extends BaseFragment {
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.vote)
    TextView vote;
    @BindView(R.id.first_air_date)
    TextView firstAriDate;
    @BindView(R.id.created_by_title)
    TextView createdByTitle;
    @BindView(R.id.genres_title)
    TextView genresTitle;
    @BindView(R.id.created_by)
    TextView createdBy;
    @BindView(R.id.genres)
    TextView genres;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.overview)
    TextView overview;
    @BindView(R.id.similar_shows)
    RecyclerView similarShowsRecyclerView;

    @BindString(R.string.tmdb_api_key)
    String apiKey;
    @BindString(R.string.error_server)
    String errorServer;

    private TvShow show;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_details, container, false);
        ButterKnife.bind(this, view);
        requestDetails(getArguments().getInt("show_id"));

        return view;
    }

    /**
     * Makes the call to the API to get the details of the desired show
     *
     * @param id int id of the show
     */
    private void requestDetails(int id) {
        mActivity.getApiService().getShowDetails(id, apiKey).enqueue(new Callback<TvShow>() {
            @Override
            public void onResponse(@NonNull Call<TvShow> call, @NonNull Response<TvShow> response) {
                if (response.isSuccessful()) {
                    show = response.body();
                    fillDetailsView();
                    getSimilarShows(show.getId());
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toasty.error(mActivity, error.getStatusMessage(), Toast.LENGTH_SHORT, true).show();
                    Log.d("API error message", error.getStatusMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TvShow> call, @NonNull Throwable t) {
                Log.d("Failure", "Api failed " + t.getMessage());
                Toasty.error(mActivity, errorServer, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    /**
     * Fills the view with the Show details
     */
    private void fillDetailsView() {
        name.setText(show.getName());
        firstAriDate.setText(show.getFirstAirDate());
        vote.setText(String.valueOf(show.getVoteAverage()));
        overview.setText(show.getOverview());
        // Build genres and creators string to be shown on the view
        StringBuilder stringBuilder = new StringBuilder();
        if (show.getGenres().size() > 0) {
            for (Genre genre : show.getGenres()) {
                if (stringBuilder.length() > 0) stringBuilder.append(", ");
                stringBuilder.append(genre.getName());
            }
            genres.setText(stringBuilder.toString());
            //clear stringBuilder
            stringBuilder.setLength(0);
        } else {
            genresTitle.setVisibility(View.GONE);
            genres.setVisibility(View.GONE);
        }
        if (show.getCreatedBy().size() > 0) {
            for (ShowCreator showCreator : show.getCreatedBy()) {
                if (stringBuilder.length() > 0) stringBuilder.append(", ");
                stringBuilder.append(showCreator.getName());
            }
            createdBy.setText(stringBuilder.toString());
        } else {
            createdByTitle.setVisibility(View.GONE);
            createdBy.setVisibility(View.GONE);
        }
        Picasso.with(mActivity)
                .load(mActivity.getResources().getString(R.string.tmbd_image_base_url) + show.getBackdropPath())
                .fit()
                .centerCrop()
                .into(image);
    }

    /**
     * Makes the call to the API to get similar shows to the desired show
     *
     * @param id int id of the show
     */

    private void getSimilarShows(int id) {
        mActivity.getApiService().getSimilarShows(id, apiKey).enqueue(new Callback<TvShowsListResponse>() {
            @Override
            public void onResponse(@NonNull Call<TvShowsListResponse> call, @NonNull Response<TvShowsListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        setSimilarAdapter(response.body().getResults());
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Toasty.error(mActivity, error.getStatusMessage(), Toast.LENGTH_SHORT, true).show();
                        Log.d("API error message", error.getStatusMessage());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Toasty.error(mActivity, errorServer, Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<TvShowsListResponse> call, @NonNull Throwable t) {
                Log.d("Failure", "Api failed " + t.getMessage());
                Toasty.error(mActivity, errorServer, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    /**
     * Sets the adapter for the horizontal RecyclerView with similar shows
     *
     * @param tvShows ArrayList of the shows
     */
    private void setSimilarAdapter(ArrayList tvShows) {
        if (tvShows != null) {
            LinearLayoutManager llm = new LinearLayoutManager(mActivity);
            llm.setOrientation(LinearLayoutManager.HORIZONTAL);
            similarShowsRecyclerView.setLayoutManager(llm);
            // We could also paginate this RecyclerView
            TvShowsListAdapter showsAdapter = new TvShowsListAdapter(mActivity, tvShows, mActivity);
            showsAdapter.notifyDataSetChanged();
            similarShowsRecyclerView.setAdapter(showsAdapter);
        }
    }
}

