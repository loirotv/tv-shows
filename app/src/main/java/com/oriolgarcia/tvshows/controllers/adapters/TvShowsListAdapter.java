package com.oriolgarcia.tvshows.controllers.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oriolgarcia.tvshows.MainActivity;
import com.oriolgarcia.tvshows.R;
import com.oriolgarcia.tvshows.controllers.models.TvShow;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Adapter used in the shows RecyclerView
 * Created by Oriol on 10/02/2018.
 */

public class TvShowsListAdapter extends RecyclerView.Adapter {
    private MainActivity mActivity;
    private ArrayList data = new ArrayList();
    private Context mContext;

    public TvShowsListAdapter(Context mContext, ArrayList data, MainActivity mActivity) {
        this.mActivity = mActivity;
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_tv_show, parent, false);
        return new TvShowHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder.getClass() == TvShowHolder.class) {
            final TvShowHolder tvShowHolder = (TvShowHolder) holder;
            final TvShow tvShow = (TvShow) data.get(position);
            if (tvShow != null) {
                tvShowHolder.name.setText(tvShow.getName());
                tvShowHolder.vote.setText(String.valueOf(tvShow.getVoteAverage()));
                tvShowHolder.overview.setText(tvShow.getOverview());
                Picasso.with(mContext)
                        .load(mActivity.getResources().getString(R.string.tmbd_image_base_url) + tvShow.getPosterPath())
                        .fit()
                        .into(tvShowHolder.poster);
                tvShowHolder.item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Open details of the selected show
                        Bundle bundle = new Bundle();
                        bundle.putInt("show_id", tvShow.getId());
                        mActivity.setFragment(MainActivity.SHOW_DETAILS_FRAGMENT, bundle);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class TvShowHolder extends RecyclerView.ViewHolder {
        private TextView name, vote, overview;
        private ImageView poster;
        private RelativeLayout item;

        private TvShowHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            vote = itemView.findViewById(R.id.vote);
            overview = itemView.findViewById(R.id.overview);
            poster = itemView.findViewById(R.id.poster);
            item = itemView.findViewById(R.id.item_relative_layout);
        }
    }
}
