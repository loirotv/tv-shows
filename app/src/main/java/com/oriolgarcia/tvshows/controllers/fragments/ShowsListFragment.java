package com.oriolgarcia.tvshows.controllers.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.oriolgarcia.tvshows.R;
import com.oriolgarcia.tvshows.api_client.ErrorUtils;
import com.oriolgarcia.tvshows.api_client.response.APIError;
import com.oriolgarcia.tvshows.api_client.response.TvShowsListResponse;
import com.oriolgarcia.tvshows.controllers.adapters.TvShowsListAdapter;
import com.oriolgarcia.tvshows.controllers.listeners.EndlessRecyclerViewScrollListener;
import com.oriolgarcia.tvshows.controllers.models.TvShow;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Fragment with a list that shows the most popular TV Shows
 * Created by Oriol on 10/02/2018.
 */

public class ShowsListFragment extends BaseFragment {
    @BindView(R.id.loading_indicator)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.shows_list_recycler_view)
    RecyclerView showsListRecyclerView;

    @BindString(R.string.tmdb_api_key)
    String apiKey;
    @BindString(R.string.error_server)
    String errorServer;

    private ArrayList<TvShow> tvShows = new ArrayList<>();
    private TvShowsListAdapter showsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shows_list, container, false);
        ButterKnife.bind(this, view);
        loadingIndicatorView.show();
        // get first page of shows
        getTvShows(1);
        setTvShowsAdapter(tvShows);
        return view;
    }

    /**
     * Makes call to the API to get a list of popular shows
     *
     * @param page pagination of the list
     */
    private void getTvShows(int page) {
        final int _page = page;
        mActivity.getApiService().getTvPopularShows(apiKey, page).enqueue(new Callback<TvShowsListResponse>() {
            @Override
            public void onResponse(@NonNull Call<TvShowsListResponse> call, @NonNull Response<TvShowsListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        tvShows.addAll(response.body().getResults());
                        if (_page == 1) {
                            setTvShowsAdapter(tvShows);
                            showsListRecyclerView.setVisibility(View.VISIBLE);
                        }
                        showsAdapter.notifyDataSetChanged();
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Toasty.error(mActivity, error.getStatusMessage(), Toast.LENGTH_SHORT, true).show();
                        Log.d("API error message", error.getStatusMessage());
                    }
                    loadingIndicatorView.smoothToHide();
                } catch (NullPointerException e) {
                    loadingIndicatorView.smoothToHide();
                    Toasty.error(mActivity, errorServer, Toast.LENGTH_SHORT, true).show();
                    Log.e("downloadDBContent", e.toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TvShowsListResponse> call, @NonNull Throwable t) {
                Log.d("Failure", "Api failed " + t.getMessage());
                Toasty.error(mActivity, errorServer, Toast.LENGTH_SHORT, true).show();
                loadingIndicatorView.smoothToHide();
            }
        });
    }

    /**
     * Sets adapter for the list of shows
     *
     * @param tvShows list of shows
     */
    private void setTvShowsAdapter(ArrayList<TvShow> tvShows) {
        if (tvShows != null) {
            LinearLayoutManager llm = new LinearLayoutManager(mActivity);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            showsListRecyclerView.setLayoutManager(llm);
            EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(llm) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    // Triggered only when new data needs to be appended to the list
                    getTvShows(page);
                }
            };
            showsListRecyclerView.addOnScrollListener(scrollListener);
            showsAdapter = new TvShowsListAdapter(mActivity, tvShows, mActivity);
            showsAdapter.notifyDataSetChanged();
            showsListRecyclerView.setAdapter(showsAdapter);
        }
    }
}
