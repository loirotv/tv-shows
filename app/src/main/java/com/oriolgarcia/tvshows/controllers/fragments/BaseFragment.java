package com.oriolgarcia.tvshows.controllers.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oriolgarcia.tvshows.MainActivity;

/**
 * BaseFragment
 * Created by Oriol on 10/02/2018.
 */

public class BaseFragment extends Fragment {
    protected MainActivity mActivity;

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setFragment(this);
    }
}
