package com.oriolgarcia.tvshows;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.oriolgarcia.tvshows.api_client.ApiService;
import com.oriolgarcia.tvshows.api_client.ApiUtils;
import com.oriolgarcia.tvshows.controllers.fragments.*;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_container)
    FrameLayout mainContainer;

    private FragmentManager fm = getFragmentManager();
    private Fragment mFragment;
    private ApiService apiService;

    /*
   Constant identifiers of Fragments
    */
    public static final int SHOWS_LIST_FRAGMENT = 1;
    public static final int SHOW_DETAILS_FRAGMENT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        apiService = ApiUtils.getApiService();

        setFragment(SHOWS_LIST_FRAGMENT, null);
    }

    /**
     * Shows the fragment selected by caseNum
     *
     * @param caseNum id of the desired Fragment to be shown
     */
    public void setFragment(int caseNum, Bundle bundle) {
        FragmentTransaction ft = fm.beginTransaction();
        Fragment frag;

        switch (caseNum) {
            case SHOWS_LIST_FRAGMENT:
                frag = new ShowsListFragment();
                break;
            case SHOW_DETAILS_FRAGMENT:
                frag = new ShowDetailsFragment();
                ft.addToBackStack(null);
                break;
            default:
                frag = new ShowsListFragment();
                break;
        }

        frag.setArguments(bundle);
        ft.replace(R.id.main_container, frag);
        ft.commitAllowingStateLoss();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            slideFragment();
        }
    }

    /**
     * Animate fragments transaction
     */
    @RequiresApi(21)
    private void slideFragment() {
        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.START);
        TransitionManager.beginDelayedTransition(mainContainer, slide);
    }

    public void setFragment(Fragment fragment) {
        mFragment = fragment;
    }

    public Fragment getFragment() {
        return mFragment;
    }

    public ApiService getApiService() {
        return apiService;
    }

    public void setApiService(ApiService apiService) {
        this.apiService = apiService;
    }
}
